# Beginner guide

## How to contribute to tools repo

### Register to Gitlab.com

Goto: https://gitlab.com/users/sign_up

![Register Gitlab](./img/01_Register_Gitlab.png)

Fill the form on the right of the page and clic on "Register" button.

You should received an email with a link to confirm your email.
___

### Connect to Gitlab.com

Goto: https://gitlab.com/users/sign_in

![Sign In Gitlab](./img/02_SignIn_Gitlab.png)

Enter your email and password and clic on "Sign In"
___

### Fork the tools Repository

Goto: https://gitlab.com/ifb-elixirfr/cluster/tools

Clic on the "Fork" button (top right in the page)

![Fork Tools](./img/03_Fork_Tools.png)

Then select the place where fork will be stored, clic on your username:

![Select Name Space](./img/04_Select_NameSpace.png)

You should see a message telling you what it have worked in the blue box:

![Check Fork](./img/05_Check_Fork.png)
___

### Find a conda package to add

Goto: https://anaconda.org/search

![Search Conda](./img/06_Search_Conda.png)

Type the name of the package you are looking for and clic on the green search button

Note the name and the version of the package, here it is :
- name: berokka
- version 0.2.3
___

### Create a branch on your fork

Goto: https://gitlab.com/(Your Gitlab Name)/tools

Clic on the "+" button and select "New branch"

![New Branch](./img/07_New_Branch.png)

Choose a name related to the package you add

![Create Branch](./img/08_Create_Branch.png)
___

### Create a directory for the package

Clic on the "+" button and select "New directory"

![New Directory](./img/07_New_Directory.png)

The directory name must be /tools/(the package name)/(the package version)

![Create Directory](./img/08_Create_Directory.png)

Double check "Target Branch" should be the one you created in previous step
___

### Create a file to describe the conda package to install

Clic on the "+" button and select "New File"

![New File](./img/09_New_File.png)

The file name must be env.yml

![Create File](./img/10_Create_File.png)

The content should be like:

```
name: berokka-0.2.3
channels:
- bioconda
- conda-forge
- defaults
dependencies:
- berokka=0.2.3
```

Replace the package name and version

Double check "Target Branch" should be the one you created in previous step

Then clic on the green "Commit" button

You should have a colored result like this:

![Check File](./img/11_Check_File.png)
___

### Submit a merge request with the new package

Goto: https://gitlab.com/FanchTestIFB/tools/-/merge_requests/new

![Create MR](./img/12_Create_MR.png)

Source branch, should be on your gitlab account, and the branch you created (here it is add-berokka)

Target branch, should be on tools account, and master

Then clic on "Compare branches and continue"

Choose a smart title, like "Add berokka"

![Submit MR](./img/13_Submit_MR.png)

You must check the box to "Allow commits from members ..."

Then clic on "Submit merge request"
___

### Wait for comment

It's done, congratulation, you should now wait for comment from @ifbot or other contributor
